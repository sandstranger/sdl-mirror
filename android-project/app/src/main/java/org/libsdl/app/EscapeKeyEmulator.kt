package org.libsdl.app

import android.view.KeyEvent

const val ESCAPE_KEYCODE = 4

fun onEscapeBtnClicked (keyCode : Int){
    if (!keyCode.equals(ESCAPE_KEYCODE)) return

    val escapeKeyCode = KeyEvent.KEYCODE_ESCAPE
    SDLActivity.onNativeKeyDown(escapeKeyCode)
    SDLActivity.onNativeKeyUp(escapeKeyCode)
}
